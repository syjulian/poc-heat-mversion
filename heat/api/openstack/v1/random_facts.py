from heat.api.openstack.v1 import versioned_object
from heat.common import serializers
from heat.common import wsgi


class RandomFactsController(versioned_object.VersionedObject):
    def __init__(self, options):
        self.options = options

    @versioned_object.VersionedObject.api_version("1.0")
    def random_facts(self, req):
        fact = {'fact': ['Microversioning may or may not be working...']}
        return fact

    @versioned_object.VersionedObject.api_version("1.1")  # noqa
    def random_facts(self, req):
        fact = {'fact': ['Microversioning is sort of working...']}
        return fact

    @versioned_object.VersionedObject.api_version("1.2", "1.4")
    def facts(self, req):
        fact = {'fact': ['Racecar is racecar backwards...']}
        return fact

    @versioned_object.VersionedObject.api_version("1.5")  # noqa
    def facts(self, req):
        fact = {'fact': ['QA stands for quality assurance...']}
        return fact


def create_resource(options):
    deserializer = wsgi.JSONRequestDeserializer()
    serializer = serializers.JSONResponseSerializer()
    return wsgi.Resource(RandomFactsController(options), deserializer,
                         serializer)
